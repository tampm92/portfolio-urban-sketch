# urban-sketch

> A Vue.js project

## Setup

``` bash
# install dependencies
npm install
```

## Build Web

``` bash
# serve with hot reload at localhost:8080
npm run dev

# build for stage
npm run build
# build for production
npm run build-prod
```
