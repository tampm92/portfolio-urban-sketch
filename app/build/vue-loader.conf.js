'use strict'
const utils = require('./utils')
// const config = require('../config/index.cordova')

var config = undefined

if (process.env.PLATFORM_ENV == 'cordova') {
  config = require('../config/index.cordova')
} else {
  config = require('../config')
}

const isProduction = process.env.NODE_ENV === 'dev' && process.env.PLATFORM_ENV !== 'cordova'
const sourceMapEnabled = isProduction
  ? config.dev.productionSourceMap
  : config.build.cssSourceMap

module.exports = {
  loaders: utils.cssLoaders({
    sourceMap: sourceMapEnabled,
    extract: isProduction
  }),
  cssSourceMap: sourceMapEnabled,
  cacheBusting: config.dev.cacheBusting,
  transformToRequire: {
    video: ['src', 'poster'],
    source: 'src',
    img: 'src',
    image: 'xlink:href'
  }
}
