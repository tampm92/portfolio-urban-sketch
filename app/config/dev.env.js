'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"dev"',
  PLATFORM_ENV: '"web"',
  APP_NAME: '"Urban Sketch"',
  API_BASE_URL: '""',
  API_SEARCH_URL: '""',
  DEFAULT: `{
    location: {longitude: 12.4922309, latitude: 41.8902102}
  }`,
  MAPBOX: `{
    accessToken: "pk.eyJ1IjoibmFkaXIwMDIiLCJhIjoiY2puaWY3djNqMG8wbzN3cXRtZnMwenlzOCJ9.Beichz_sLqqEBwhK2Lzf1g",
    defaultStyle: "mapbox://styles/mapbox/light-v9"
  }`,
  FACEBOOK: `{
    appId: ""
  }`,
  GOOGLE: `{
    appId: ""
  }`,
  AWS: `{
    urlPreSigned: '',
    urlS3: '',
  }`
})
