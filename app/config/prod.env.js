'use strict'
module.exports = {
  NODE_ENV: '"production"',
  APP_NAME: '"Urban Sketch"',
  API_BASE_URL: '""',
  API_SEARCH_URL: '""',
  MAIL_SUPPORT : '"contact@demo.io"',
  DEFAULT: `{
    location: {longitude: 12.4922309, latitude: 41.8902102}
  }`,
  FACEBOOK: `{
    appId: ""
  }`,
  GOOGLE: `{
    appId: ""
  }`,
  MAPBOX: `{
    accessToken: "pk.eyJ1IjoibmFkaXIwMDIiLCJhIjoiY2puaWY3djNqMG8wbzN3cXRtZnMwenlzOCJ9.Beichz_sLqqEBwhK2Lzf1g",
    defaultStyle: "mapbox://styles/mapbox/light-v9"
  }`,
  AWS: `{
    urlPreSigned: '',
    urlS3: '',
  }`
}
