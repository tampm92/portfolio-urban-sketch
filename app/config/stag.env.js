'use strict'
module.exports = {
  NODE_ENV: '"stag"',
  APP_NAME: '"Urban Sketch"',
  API_BASE_URL: '""',
  API_SEARCH_URL: '""',
  MAIL_SUPPORT : '"contact@demo.io"',
  DEFAULT: `{
    location: {longitude: 106.65861679999999, latitude: 10.7642177}
  }`,
  FACEBOOK: `{
    appId: ""
  }`,
  GOOGLE: `{
    appId: ""
  }`,
  MAPBOX: `{
    accessToken: "pk.eyJ1IjoibmFkaXIwMDIiLCJhIjoiY2puaWY3djNqMG8wbzN3cXRtZnMwenlzOCJ9.Beichz_sLqqEBwhK2Lzf1g",
    defaultStyle: "mapbox://styles/mapbox/light-v9"
  }`,
  AWS: `{
    urlPreSigned: '',
    urlS3: '',
  }`
}
