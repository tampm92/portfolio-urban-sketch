// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import store from './share/store/index'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import AuthService from './share/services/AuthService'
import VueNotifications from 'vue-notification'
import velocity from 'velocity-animate'
import vSelect from 'vue-select'
import Logger from './utils/logger'
import Notification from './utils/notification'
import Util from './utils/util'
import cLoader from './components/c-loader/cLoader.vue'
import Layout from './layouts/Layout'
import LayoutDefault from './layouts/LayoutDefault.vue'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueNotifications, {
  velocity
})
Vue.component('layout', Layout)
Vue.component('LayoutDefault', LayoutDefault)
Vue.component('v-select', vSelect)
Vue.component('loader', cLoader)
Vue.use(require('vue-moment'));
// global library setup
Object.defineProperty(Vue.prototype, '$Logger', { value: Logger });
Object.defineProperty(Vue.prototype, '$Notification', { value: Notification });
Object.defineProperty(Vue.prototype, '$Util', { value: Util });

const authService = new AuthService()

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let isLogin = authService.isLoggedIn();

    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!isLogin) {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  created() {
  },
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
