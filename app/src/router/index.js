import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home/Home'
import Login from '@/views/Login/Login'
import Test from '@/views/Test/Test'
import SidePanel from '@/views/SidePanel/SidePanel'
import Upload from '../views/Upload/Upload.vue'
import UploadEdit from '../views/UploadEdit/UploadEdit.vue'
import Search from '../views/Search/Search.vue'
import CurrentProfile from '../views/CurrentProfile/CurrentProfile.vue'
import UpdateCurrentProfile from '../views/UpdateCurrentProfile/UpdateCurrentProfile.vue'
import AccountMenu from '../views/AccountMenu/AccountMenu.vue'
import Profile from '../views/Profile/Profile.vue'
import CurrentFeed from '../views/CurrentFeed/CurrentFeed.vue'
import EditCurrentFeed from '../views/EditCurrentFeed/EditCurrentFeed.vue'
import Notifications from '../views/Notifications/Notifications.vue'
import Followers from '../views/Followers/Followers.vue'
import Following from '../views/Following/Following.vue'
import Points from '../views/Points/Points.vue'
import Settings from '../views/Settings/Settings.vue'
import Privacy from '../views/Privacy/Privacy.vue'
import About from '../views/About/About.vue'
import Support from '../views/Support/Support.vue'
import Tutorial from '../views/Tutorial/Tutorial.vue'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/currentFeed',
      name: 'CurrentFeed',
      component: CurrentFeed,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/editCurrentFeed',
      name: 'EditCurrentFeed',
      component: EditCurrentFeed,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/notifications',
      name: 'Notifications',
      component: Notifications,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/currentProfile',
      name: 'CurrentProfile',
      component: CurrentProfile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/updateCurrentProfile',
      name: 'UpdateCurrentProfile',
      component: UpdateCurrentProfile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile/:userId',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/accountMenu',
      name: 'AccountMenu',
      component: AccountMenu,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/followers',
      name: 'Followers',
      component: Followers,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/following',
      name: 'Following',
      component: Following,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/points',
      name: 'Points',
      component: Points,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/privacy',
      name: 'Privacy',
      component: Privacy,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/support',
      name: 'Support',
      component: Support,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/tutorial/:step',
      name: 'Tutorial',
      component: Tutorial
    },
    {
      path: '/sidePanel/:sketchId',
      name: 'SidePanel',
      component: SidePanel
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/upload/:sketchId',
      name: 'UploadEdit',
      component: UploadEdit,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    {
      path: '/test',
      name: 'Test',
      component: Test
    }
  ]
})
