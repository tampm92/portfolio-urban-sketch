import axios from 'axios';

let axiosApiTemp = axios.create({
    baseURL: `/api/` // Api base url place here
});

// Auto get token from cookie to free reload
axiosApiTemp.interceptors.request.use(function (config) {
    // Process adding token to header place here
    // ---------- Todo: Process token to add to header of request
    // To get the value of a cookie use
    // let token = Vue.cookie.get('token');
    // if (token) {
    //     // Here is the format of the header axios
    //     config.headers = {Authorization: `Bearer ${token}`}
    // }
    // After check that has token or not, return the config file
    return config
}, function (error) {
    return Promise.reject(error)
});

// Preprocess that every request if return status error 401 (UnAuthorize)
// It will return to login page
// Note: Token may be expire or not correct token
axiosApiTemp.interceptors.response.use(null, (error) => {
    // Error process place here
    // ---------- Todo: Error process place here
    return Promise.reject(error)
});

// Return the previous config Axios
export var axiosApi = axiosApiTemp

// Api for auth request
export const axiosAuthApi = axios.create({
    // Testing base url
    baseURL: process.env.API_BASE_URL
    // baseURL: `/api/` // Base Url place here
})
