export default class Comment {
    constructor() {
        this.id = '',
        this.email = '',
        this.name = '',
        this.imageUrl = '',
        this.value = '',
        this.userId = ''
    }
}
