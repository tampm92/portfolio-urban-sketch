export class Follower {
    constructor() {
      this.followingUserId = '',
      this.followerUserId = '',
      this.followingUserName = '',
      this.imageUrl = '',
      this.value = 0,
      this.blockFollower = 0
    }
}

export class Following {
  constructor() {
    this.followingUserId = '',
    this.followerUserId = '',
    this.followerUserName = '',
    this.imageUrl = '',
    this.value = 0,
    this.blockFollower = 0
  }
}
