export default class Pin {
    constructor() {
        this.id = '',
        this.name = '',
        this.longitude = '',
        this.latitude = '',
        this.createdAt = '',
        this.updatedAt = '',
        this.sketches = []
    }
}
