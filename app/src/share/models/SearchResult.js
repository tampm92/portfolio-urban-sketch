export default class SearchResult {
  constructor() {
      this.id = '',
      this.name = '',
      this.type = '',
      this.metadata = null
  }
}
