export default class Sketch {
    constructor() {
        this.id = '',
        this.name = '',
        this.url = '',
        this.userId = '',
        this.pinId = '',
        this.pinName = '',
        this.longitude = '',
        this.latitude = '',
        this.createdAt = '',
        this.updatedAt = '',
        this.user = {},
        this.likes = [],
        this.favourites = [],
        this.comments = []
    }
}
