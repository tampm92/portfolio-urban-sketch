export default class User {
    constructor() {
        this.id = '',
        this.email = '',
        this.bio = '',
        this.website = '',
        this.insta_handle = '',
        this.email = '',
        this.name = '',
        this.imageUrl = '',
        this.fbAuthToken = '',
        this.createdAt = '',
        this.updatedAt = '',
        this.followerCount = 0,
        this.followingCount = 0,
        this.tutorialSeen = 0
    }
}
