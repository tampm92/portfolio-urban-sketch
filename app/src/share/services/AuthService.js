import LocalStorage from '../../utils/local-storage'

const userIdKey = 'userId';

export default class AuthService {
  isLoggedIn() {
    return LocalStorage.isKey(userIdKey);
  }

  /**
   *
   * @param {id} data
   */
  saveAuth(data) {
    LocalStorage.set(userIdKey, data.id)
  }

  /**
   * return id
   */
  getAuth() {
    return LocalStorage.get(userIdKey);
  }

  /**
   * return id
   */
  deleteAuth() {
    return LocalStorage.remove(userIdKey);
  }
}
