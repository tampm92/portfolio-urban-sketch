import {
    axiosAuthApi
} from '../axios-config/axios-config';

export default class BackendService {
    constructor () {
        this.axiosAuthApi = axiosAuthApi
    }
}