import BackendService from './BackendService';
import Comment from '../models/Comment';

export default class CommentService extends BackendService {
  convertComment(data) {
    var comment = new Comment();
    comment.id = data.comment_id;
    comment.name = data.name;
    comment.email = data.email;
    comment.imageUrl = data.image_url;
    comment.value = data.value;
    comment.userId = data.id;

    return comment;
  }

  /**
   *
   * @param {sketchId, userId, value} item
   */
  add(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  delete(commentId) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }
}
