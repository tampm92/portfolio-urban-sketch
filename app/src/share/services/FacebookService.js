import {
  axiosApi,
  axiosAuthApi
} from '../axios-config/axios-config'
import User from '../models/User';

export default class FacebookService {
  /**
   *
   * @param {string} accessToken
   * return User
   */
  getCurrentInfo(accessToken) {
    return new Promise((resolve, reject) => {
      axiosApi.get('https://graph.facebook.com/me?fields=email,first_name,last_name,picture,name', {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          }
        })
        .then((response) => {
          var data = response.data;
          var user = new User();
          user.id = data.id;
          user.email = data.email;
          user.name = data.name;
          user.imageUrl = data.picture ? data.picture.data.url : undefined;
          user.fbAuthToken = accessToken;

          this.getPicture(data.id, 'large', accessToken)
            .then((responseGetPicture) => {
              user.imageUrl = responseGetPicture;
              return resolve(user);
            })
            .catch(error => {
              return reject(error);
            })
        })
        .catch(error => {
          return reject(error);
        });
    })
  }

  /**
   *
   * @param {string} facebookId
   * @param {string} typePicture // large, normal, small, square
   * @param {*} accessToken
   */
  getPicture(facebookId, typePicture, accessToken) {
    return new Promise((resolve, reject) => {
      axiosApi.get(`https://graph.facebook.com/${facebookId}/picture?type=${typePicture}&redirect=false`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          }
        })
        .then((response) => {
          return resolve(response.data.data.url);
        })
        .catch(error => {
          reject(error)
        });
    })
  }
}
