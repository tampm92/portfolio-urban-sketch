import BackendService from './BackendService';
import Favourite from '../models/Favourite';

export default class FavouriteService extends BackendService {
  convertFavourite(data) {
    var favourite = new Favourite();
    favourite.id = data.id;
    favourite.name = data.name;
    favourite.email = data.email;
    favourite.imageUrl = data.image_url;

    return favourite;
  }

  /**
   *
   * @param {pinId, userId, value} item
   */
  add(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }
}
