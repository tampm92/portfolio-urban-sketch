import BackendService from './BackendService';
import Feed from '../models/Feed';
import FakeFeed from '../../utils/fake-date/feed'

export default class FeedService extends BackendService {
  convertFeed(favourites, followedUsers) {
    var feed = new Feed();

    for(var item in favourites) {
      feed.favourites.push(this.convertFavorite(favourites[item]));
    }

    for(var item in followedUsers) {
      feed.followedUsers.push(this.convertFollowedUser(followedUsers[item]));
    }

    return feed;
  }
  convertFavorite(data) {
    var favorite = {
      pinInfo: {},
      sketches: []
    };
    favorite.pinInfo = this.convertPinInfo(data.pin_info);
    data.sketches.forEach(item => {
      favorite.sketches.push(this.convertSketch(item));
    });

    return favorite;
  }
  convertPinInfo(data) {
    var pinInfo = {};
    pinInfo.id = data.id;
    pinInfo.name = data.name;
    pinInfo.longitude = data.longitude;
    pinInfo.latitude = data.latitude;
    pinInfo.createdAt = data.created_at;
    pinInfo.updatedAt = data.updated_at;

    return pinInfo;
  }
  convertFollowedUser(data) {
    var followedUser = {
      userInfo: {},
      sketches: []
    };
    followedUser.userInfo = this.convertUserInfo(data.userInfo);
    data.sketches.forEach(item => {
      followedUser.sketches.push(this.convertSketch(item));
    });

    return followedUser;
  }
  convertUserInfo(data) {
    var userInfo = {};
    userInfo.user_id = data.user_id;
    userInfo.name = data.name;
    userInfo.email = data.email;

    return userInfo;
  }
  convertSketch(data) {
    var sketch = {};
    sketch.id = data.id;
    sketch.name = data.name;
    sketch.url = data.url;
    sketch.pinId = data.pin_id;
    sketch.likesCount = data.likes_count;
    sketch.commentsCount = data.comments_count;

    return sketch;
  }

  /**
   * Feed Setting
   */
  convertFeedSetting(followedUsers, followedLocations) {
    var feedSetting = {
      followedUsers: [],
      followedLocations: []
    };

    followedUsers.forEach(item => {
      if (item.value == 1) {
        feedSetting.followedUsers.push(this.convertFollowedUsersFeedSetting(item));
      }
    });

    followedLocations.forEach(item => {
      if (item.value == 1) {
        feedSetting.followedLocations.push(this.convertFollowedLocationsFeedSetting(item));
      }
    });

    return feedSetting;
  }

  convertFollowedUsersFeedSetting(data) {
    var followedUser = {};
    followedUser.followingUserId = data.following_user_id;
    followedUser.followerUserId = data.follower_user_id;
    followedUser.value = data.value;
    followedUser.createdAt = data.created_at;
    followedUser.updatedAt = data.updated_at;
    followedUser.blockFollower = data.block_follower;
    followedUser.followerUsername = data.follower_user_name;
    followedUser.type = data.type;

    return followedUser;
  }

  convertFollowedLocationsFeedSetting(data) {
    var followedLocation = {};
    followedLocation.id = data.id;
    followedLocation.userId = data.user_id;
    followedLocation.sketchId = data.sketch_id;
    followedLocation.pinId = data.pin_id;
    followedLocation.value = data.value;
    followedLocation.createdAt = data.created_at;
    followedLocation.updatedAt = data.updated_at;
    followedLocation.type = data.type;
    followedLocation.name = data.name;

    return followedLocation;
  }

  convertFeedSettingsForUpdate(followedUsers, followedLocations) {
    var result = {
      settings: []
    };

    followedUsers.forEach(item => {
      result.settings.push({
        follower_user_id: item.followerUserId,
        value: item.value,
        type: item.type,
        block_follower: item.blockFollower
      });
    });

    followedLocations.forEach(item => {
      result.settings.push({
        pin_id: item.pinId,
        value: item.value,
        type: item.type,
      });
    });

    return result;
  }
  /**
   * return array Tag
   */
  getByUserId(userId) {
    return new Promise((resolve, reject) => {
      var data = FakeFeed.findByUserId(userId);
      var result = this.convertFeed(data.favourites, data.followedUsers);

      return resolve(result);
    })
  }

  getFeedSettingByUserId(userId) {
    return new Promise((resolve, reject) => {
      var data = FakeFeed.findFeedSetting(userId);
      var result = this.convertFeedSetting(data.followed_users, data.followed_locations);

      return resolve(result);
    })
  }

  updateFeedSettings(userId, followedUsers, followedLocations) {
    var dataUpdate = this.convertFeedSettingsForUpdate(followedUsers, followedLocations);
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }
}
