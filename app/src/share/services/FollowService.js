import BackendService from './BackendService';
import {Follower, Following} from '../models/Follow'
import FakeFollow from '../../utils/fake-date/follow'

export default class FollowService extends BackendService {
  convertFollower(data) {
    var follow = new Follower();
    follow.followerUserId = data.follower_user_id;
    follow.followingUserId = data.following_user_id;
    follow.followingUserName = data.following_user_name;
    follow.imageUrl = data.image_url;
    follow.value = data.value;
    follow.blockFollower = data.block_follower ? 1 : 0;

    return follow;
  }

  convertFollowing(data) {
    var follow = new Following();
    follow.followerUserId = data.follower_user_id;
    follow.followingUserId = data.following_user_id;
    follow.followerUserName = data.follower_user_name;
    follow.imageUrl = data.image_url;
    follow.value = data.value;
    follow.blockFollower = data.block_follower ? 1 : 0;

    return follow;
  }

  getFollowersByUserId(userId) {
    return new Promise((resolve, reject) => {
      var data = FakeFollow.getFollowersByUserId(userId);
      var result = [];
      data.forEach(item => {
        result.push(this.convertFollower(item));
      });

      return resolve(result);
    })
  }

  getFollowingByUserId(userId) {
    return new Promise((resolve, reject) => {
      var data = FakeFollow.getFollowingByUserId(userId);
      var result = [];
      data.forEach(item => {
        result.push(this.convertFollowing(item));
      });

      return resolve(result);
    })
  }
  /**
   *
   * @param {urlUserId, followerUserId} item
   */
  add(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  /**
   *
   * @param {userId, data} data
   */
  updateFollow(userId, data) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  /**
   *
   * @param {userId, data} data
   */
  updateBlock(userId, data) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  /**
   *
   * @param {*} followingUserId
   * @param {*} followerUserId
   */
  isFollowing(followerUserId, followingUserId) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }
}
