import BackendService from './BackendService';
import Like from '../models/Like';

export default class LikeService extends BackendService {
  convertLike(data) {
    var like = new Like();
    like.userId = data.user_id;

    return like;
  }

  /**
   *
   * @param {userId, sketchId, value} item
   * value: 1-like , 0-unlike
   */
  add(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }
}
