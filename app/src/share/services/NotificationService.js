import BackendService from './BackendService';
import Notification from '../models/Notification';
import FakeNotification from '../../utils/fake-date/notification'

export default class NotificationService extends BackendService {
  convertNotification(feed, summary) {
    var notification = new Notification();

    feed.forEach(item => {
      notification.feed.push(this.convertNotificationFeed(item));
    });
    notification.summary = this.convertNotificationSummary(summary);

    return notification;
  }

  convertNotificationFeed(data) {
    var feed = {};
    feed.id = data.id;
    feed.type = data.type;
    feed.name = data.name;
    feed.imageUrl = data.image_url;
    feed.createdAt = data.created_at;

    return feed;
  }
  convertNotificationSummary(data) {
    var summary = {};
    summary.followers = data.followers;
    summary.following = data.following;

    return summary;
  }
  /**
   * return array Tag
   */
  getByUserId(userId) {
    return new Promise((resolve, reject) => {
      var data = FakeNotification.getNotificationFeedByUserId(userId);
      var result = this.convertNotification(data.feed, data.summary);
      return resolve(result);
    })
  }
}
