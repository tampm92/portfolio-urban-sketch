import BackendService from './BackendService';
import Pin from '../models/Pin';
import {
  SketchService
} from '.'
import FakePin from '../../utils/fake-date/pin'

export default class PinService extends BackendService {
  convertPin(data, sketches = []) {
    var pin = new Pin();
    pin.id = data.id;
    pin.name = data.name;
    pin.longitude = data.longitude;
    pin.latitude = data.latitude;
    pin.createdAt = data.created_at;
    pin.updatedAt = data.updated_at;
    pin.sketches = [];

    sketches.forEach(item => {
      pin.sketches.push(SketchService.convertSketch(item.sketch_info, item.user, item.comments, item.favourites, item.likes));
    });

    return pin;
  }
  /**
   *
   * @param {tags} query
   */
  getAll() {
    return new Promise((resolve, reject) => {
      var data = FakePin.getAll();
      var result = [];

      data.forEach(item => {
        if (item.pin_info && item.sketches) {
          result.push(this.convertPin(item.pin_info, item.sketches));
        }
      });

      return resolve(result);
    })

  }

  getPinById(pinId, userId) {
    return new Promise((resolve, reject) => {
      var data = FakePin.findById(pinId, userId);
      var result = {};
      if (data && data.length) {
        result = this.convertPin(data[0].pin_info, data, data[0]);
        result.isPinFavourited = data[0].is_pin_favourited;
      }
      return resolve(result);

    })
  }
}
