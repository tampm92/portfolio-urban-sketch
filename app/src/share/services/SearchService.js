import {
  axiosApi
} from '../axios-config/axios-config'
import SearchResult from '../models/SearchResult'
import MapTool from '../../utils/map-tool'

export default class SearchService {
  convertSearchResult(data) {
    var result = new SearchResult();
    result.id = data.id;
    result.name = data.fields.name;
    result.type = data.fields.type;

    return result;
  }

  convertMapSearchResult(data) {
    var result = new SearchResult();
    result.id = data.id;
    result.name = data.place_name;
    result.metadata = {
      center: data.center
    };
    result.type = 'map';

    return result;
  }
  /**
   *
   * @param {string} type (is tag/sketch/user)
   * return SearchResult
   */
  search(query) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })

  }

}
