import BackendService from './BackendService';
import Sketch from '../models/Sketch';
import { UserService, CommentService, FavouriteService, LikeService, TagService } from '.';
import FakeSketch from '../../utils/fake-date/sketch'

export default class SketchService extends BackendService {
  convertSketch(data, users = [], comments = [], favourites = [], likes = [], tags = []) {
    var sketch = new Sketch();
    sketch.id = data.id;
    sketch.name = data.name;
    sketch.url = data.url;
    sketch.userId = data.user_id;
    sketch.pinId = !!!users || users.length == 0 ?  null : users[0].pin_id;
    sketch.pinName = data.pin_name;
    sketch.longitude = data.longitude;
    sketch.latitude = data.latitude;
    sketch.createdAt = data.created_at;
    sketch.updatedAt = data.updated_at;
    sketch.user = !!!users || users.length == 0 ?  {} : UserService.convertUserForSketch(users[0]);
    sketch.comments = [];
    sketch.favourites = [];
    sketch.likes = [];
    sketch.tags = [];

    comments.forEach(item => {
      sketch.comments.push(CommentService.convertComment(item));
    });

    favourites.forEach(item => {
      sketch.favourites.push(FavouriteService.convertFavourite(item));
    });

    likes.forEach(item => {
      sketch.likes.push(LikeService.convertLike(item));
    });

    tags.forEach(item => {
      sketch.tags.push(TagService.convertTag(item));
    });

    return sketch;
  }

  /**
   *
   * @param {*} sketchId
   */
  getById(sketchId) {
    return new Promise((resolve, reject) => {
      var data = FakeSketch.findById(sketchId);
      console.log('sketchId', sketchId, data)
      var result = this.convertSketch(data.sketch_info, data.user, data.comments, data.favourites, data.likes, data.tags);

      return resolve(result);
    })
  }

  /**
   *
   * @param {lat, long, radius, tags} query
   */
  getAll(query) {
    return new Promise((resolve, reject) => {
      var data = FakeSketch.getAllSketches();
      var result = [];
      data.forEach(item => {
        result.push(this.convertSketch(item.sketch_info));
      });

      return resolve(result);
    })
  }

  /**
   *
   * @param {Sketch} item
   * return sketch id
   */
  add(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  /**
   *
   * @param {Sketch} item
   * return sketch id
   */
  update(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  /**
   *
   * @param {sketchId} sketchId
   */
  delete(sketchId) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

}
