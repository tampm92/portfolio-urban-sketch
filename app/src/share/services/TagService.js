import BackendService from './BackendService';
import Tag from '../models/Tag';
import FakeTag from '../../utils/fake-date/tag'

export default class TagService extends BackendService {
  convertTag(data) {
    var tag = new Tag();
    tag.tag = data.tag;
    tag.createdAt = data.created_at;

    return tag;
  }
  /**
   * return array Tag
   */
  getAll() {
    return new Promise((resolve, reject) => {
      var data = FakeTag.getAll();
      var result = [];
      data.forEach(item => {
        result.push(this.convertTag(item));
      });

      return resolve(result);
    })
  }

  /**
   *
   * @param {sketchId, tags} item
   */
  add(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }

  /**
   *
   * @param {sketchId, tags} item
   */
  update(item) {
    return new Promise((resolve, reject) => {
      return resolve(true);
    })
  }
}
