import BackendService from './BackendService';
import User from '../models/User';
import {
  SketchService
} from '.'
import FakeUser from '../../utils/fake-date/user'

// const sketchService = new SketchService();

export default class UserService extends BackendService {
  convertUser(data, sketches = [], followerCount, followingCount) {
    if (!!!data.id) {
      return null;
    }

    var user = new User();
    user.id = data.id;
    user.name = data.name;
    user.email = data.email;
    user.website = data.website;
    user.insta_handle = data.insta_handle;
    user.bio = data.bio;
    user.imageUrl = data.image_url;
    user.fbAuthToken = data.fb_auth_token;
    user.sketches = [];
    user.followerCount = followerCount;
    user.followingCount = followingCount;
    user.tutorialSeen = data.tutorial_seen ? 1 : 0;

    sketches.forEach(item => {
      user.sketches.push(SketchService.convertSketch(item));
    });

    return user;
  }

  convertUserForSketch(data, sketches = [], followerCount, followingCount) {
    var user = new User();
    user.id = data.user_id ? data.user_id : data.id;
    user.name = data.name;
    user.email = data.email;
    user.website = data.website;
    user.insta_handle = data.insta_handle;
    user.bio = data.bio;
    user.imageUrl = data.image_url;
    user.fbAuthToken = data.fb_auth_token;
    user.tutorialSeen = data.tutorial_seen ? 1 : 0;

    return user;
  }

  /**
   *
   * @param {User} item
   * return results
   */
  add(item) {
    return new Promise((resolve, reject) => {
      this.axiosAuthApi.post('user', {
          id: item.id,
          email: item.email,
          name: item.name,
          image_url: item.imageUrl,
          fb_auth_token: item.fbAuthToken
        })
        .then(response => {
          var data = response.data;
          return resolve(data.results);
        })
        .catch(error => {
          return reject(error);
        });
    })
  }

  /**
   *
   * @param {string} id
   * return User
   */
  get(id) {
    return new Promise((resolve, reject) => {
      let data = FakeUser.findById(id);
      var user = null;
      if (data.user_info && data.user_info.length) {
        user = this.convertUser(data.user_info[0], data.sketches, data.follower_count, data.following_count);
      }
      return resolve(user);
    })
  }

  /**
   *
   * @param {User} item
   * return results
   */
  update(item) {
    return new Promise((resolve, reject) => {
      this.axiosAuthApi.post('user', {
          id: item.id,
          name: item.name,
          email: item.email,
          website: item.website,
          insta_handle: item.insta_handle,
          bio: item.bio,
          image_url: item.imageUrl,
          fb_auth_token: item.fbAuthToken,
          tutorial_seen: item.tutorialSeen
        })
        .then(response => {
          var data = response.data;
          return resolve(data.results);
        })
        .catch(error => {
          return reject(error);
        });
    })
  }

  /**
   *
   * @param {User} item
   * return results
   */
  addOrUpdate(item) {
    return new Promise((resolve, reject) => {
      this.get(item.id)
        .then(userGet => {
          if (userGet) {
            this.update(userGet)
              .then(dataUpdate => {
                return resolve(dataUpdate);
              })
              .catch(error => {
                return reject(error);
              });
          } else {
            this.add(item)
              .then(dataAdd => {
                return resolve(dataAdd);
              })
              .catch(error => {
                return reject(error);
              });
          }
        })
        .catch(error => {
          return reject(error);
        });
    })
  }
}
