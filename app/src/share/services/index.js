import AuthServiceType from './AuthService'
import UserServiceType from './UserService'
import CommentServiceType from './CommentService'
import FacebookServiceType from './FacebookService'
import FavouriteServiceType from './FavouriteService'
import LikeServiceType from './LikeService'
import PinServiceType from './PinService'
import SearchServiceType from './SearchService'
import SketchServiceType from './SketchService'
import TagServiceType from './TagService'
import NotificationServiceType from './NotificationService'
import FeedServiceType from './FeedService'
import FollowServiceType from './FollowService'

export const AuthService = new AuthServiceType()
export const UserService = new UserServiceType()
export const CommentService = new CommentServiceType()
export const FacebookService = new FacebookServiceType()
export const FavouriteService = new FavouriteServiceType()
export const LikeService = new LikeServiceType()
export const PinService = new PinServiceType()
export const SearchService = new SearchServiceType()
export const SketchService = new SketchServiceType()
export const TagService = new TagServiceType()
export const NotificationService = new NotificationServiceType()
export const FeedService = new FeedServiceType()
export const FollowService = new FollowServiceType()
