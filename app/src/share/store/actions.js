import * as types from './mutation-types'
import CloudStorage from '../../utils/cloud-storage'
import MapTool from '../../utils/map-tool'
import Util from '../../utils/util'
import Logger from '../../utils/logger'

export default {
  /**
   * Global
   */
  handleResizeWindow(context, actionData) {
    Logger.log(context.state.nodeEnv, context.state.platformEnv)
    var windowWidth = Util.getWidthWindow();

    let payload = {
      type: types.SET_IS_MOBILE_SIZE,
      data: windowWidth<context.state.maxHeightMobile
    };
    context.commit(payload);

    Util.handleResizeWindow((event) => {
      var windowWidth = Util.getWidthWindow();

      let payload = {
        type: types.SET_IS_MOBILE_SIZE,
        data: windowWidth<context.state.maxHeightMobile
      };
      context.commit(payload);
    })
  },
  beginLoading(context, actionData) {
    let payload = {
      type: types.BEGIN_LOADING,
      data: actionData
    };
    context.commit(payload)
  },
  stopLoading(context, actionData) {
    let payload = {
      type: types.STOP_LOADING,
      data: actionData
    };
    context.commit(payload)
  },
  // actionData = { layoutName }
  setLayoutName(context, actionData) {
    let payload = {
      type: types.SET_LAYOUT_NAME,
      data: actionData.layoutName
    };
    context.commit(payload)
  },
  // actionData = { name }
  setLeftSliderName(context, actionData) {
    let payload = {
      type: types.SET_LEFT_SLIDER,
      data: actionData.name
    };
    context.commit(payload)
  },
  // actionData = {  }
  backLeftSlider(context, actionData) {
    let payload = {
      type: types.BACK_LEFT_SLIDER,
      data: {}
    };
    context.commit(payload)
  },
  // actionData = { longitude, latitude }
  setCurrentLocation(context, actionData) {
    let payload = {
      type: types.SET_CURRENT_LOCATION,
      data: {
        longitude: actionData.longitude,
        latitude: actionData.latitude
      }
    };
    context.commit(payload)
  },

  /**
   * Map
   */
  // actionData = { container, center, zoom, interactive, isAddSearch }
  initMap(context, actionData) {
    return new Promise((resolve, reject) => {
      var config = {
        container: actionData.container,
        style: context.state.mapboxDefaultStyle,
        center: actionData.center,
        zoom: actionData.zoom,
        interactive: actionData.interactive === undefined ? true : actionData.interactive
      };
      var map = MapTool.init(context.state.mapboxAccessToken, config, actionData.isAddSearch);

      return resolve(map);
    });
  },
  // actionData = { searchString }
  mapSearch(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    });

    return new Promise((resolve, reject) => {
      MapTool.search(context.state.mapboxAccessToken, actionData.searchString)
        .then((data) => {
          context.commit({
            type: types.STOP_LOADING
          });

          return resolve(data);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          });

          return reject(error);
        });
    })
  },
  // actionData = { map, location: [lng,lat] }
  mapFlyTo(context, actionData) {
    return new Promise((resolve, reject) => {
      MapTool.flyTo(actionData.map, actionData.location);
      return resolve();
    })
  },
  // actionData = { map, marker: {name,class,location,event} }
  addMapMarker(context, actionData) {
    return new Promise((resolve, reject) => {
      var marker = MapTool.addMarker(actionData.map, actionData.marker);
      return resolve(marker);
    })
  },
  // actionData = { map, marker: {name,callback} }
  addMapMarkerOnMove(context, actionData) {
    MapTool.addMarkerOnMove(actionData.map, actionData.marker, actionData.callback)
  },
  // actionData = {}
  getCurrentLocation(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    });

    return new Promise((resolve, reject) => {
      return resolve({
        longitude: context.state.defaultLocation.longitude,
        latitude: context.state.defaultLocation.latitude
      });
    })
  },
  // actionData = {name}
  triggerClickMarkerByName(context, actionData) {
    MapTool.triggerClickMarkerByName(actionData.name);
  },

  /**
   * AWS
   */
  // actionData = { file }
  uploadFile(context, actionData) {
    return new Promise((resolve, reject) => {
      return resolve('url');
    })
  },

  /**
   * Home page
   */
  // actionData = { map }
  setHomeMap(context, actionData) {
    let payload = {
      type: types.SET_HOME_MAP,
      data: actionData.map
    };
    context.commit(payload)
  },
  // actionData = { marker }
  setHomeCurrentMarker(context, actionData) {
    let payload = {
      type: types.SET_HOME_CURRENT_MARKER,
      data: actionData.marker
    };
    context.commit(payload)
  },
  // actionData = { marker }
  pushHomeMarker(context, actionData) {
    let payload = {
      type: types.PUSH_HOME_MARKER,
      data: actionData.marker
    };
    context.commit(payload)
  },
  // actionData = {}
  clearHomeMarkers(context, actionData) {
    MapTool.clearMarkers(context.state.homeMarkers);
  },

  /**
   * Search page
   */
  // actionData = { tag }
  setSearchPageTags(context, actionData) {
    let payload = {
      type: types.SET_SEARCH_PAGE_TAGS,
      data: actionData.tag
    };
    context.commit(payload)
  },
};
