export default {
  getAppTitleUppercase: (state, getter) => {
      return state.applicationTitle.toUpperCase()
  }
}
