import * as types from '../../mutation-types'
import {
  UserService,
  AuthService,
  FacebookService
} from '../../../services'
import FacebookTool from '../../../../utils/facebook-tool'
import UserFake from '../../../../utils/fake-date/user'

export default {
  //actionData
  login(context, actionData) {
    return new Promise((resolve, reject) => {
      let user = UserFake.login();
      return resolve(user);
    })
  },
  //actionData
  logout(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      FacebookTool.logout()
        .then(data => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(data);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },
  startup(context, actionData) {
    return new Promise((resolve, reject) => {
      var isLoggedIn = AuthService.isLoggedIn();
      var userId = AuthService.getAuth();
      if (!isLoggedIn) {
        return reject(false);
      }

      let payload = {
        type: types.STARTUP,
        data: {
          isLoggedIn: isLoggedIn,
          userId: userId
        }
      }
      context.commit(payload);

      return resolve(true);
    })
  },
  // actionData = { }
  getCurrentUser(context, actionData) {
    return new Promise((resolve, reject) => {
      var userId = AuthService.getAuth();
      if (!!!userId) {
        return reject('You are not login');
      }

      UserService.get(userId)
        .then(result => {
          let payload = {
            type: types.SET_AUTH,
            data: result
          }
          context.commit(payload);

          return resolve(result);
        })
        .catch(error => {
          return reject(error);
        })
    })

  },
  // actionData = { currentUser }
  addCurrentUser(context, actionData) {
    let currentUser = actionData.currentUser;
    return new Promise((resolve, reject) => {
      AuthService.saveAuth({
        id: currentUser.id
      });

      return resolve(currentUser);
    })
  },
  // actionData = { currentUser }
  updateCurrentUser(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    let currentUser = actionData.currentUser;

    return new Promise((resolve, reject) => {
      UserService.update(currentUser)
        .then(result => {
          AuthService.saveAuth({
            id: currentUser.id
          });
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });
          return reject(error);
        })
    })
  },
  // actionData = { userId }
  deleteCurrentUser(context, actionData) {
    let payload = {
      type: types.DELETE_AUTH
    }
    context.commit(payload);
    AuthService.deleteAuth();
  },
}
