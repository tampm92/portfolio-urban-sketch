import * as types from '../../mutation-types'

import Logger from '../../../../utils/logger'

export default {
  // payload = { type, data {isLoggedIn,userId} }
  [types.STARTUP] (state, payload) {
    Logger.log(payload);
    let data = payload.data;

    state.isLoggedIn = data.isLoggedIn;
    state.id = data.userId;
  },

  // payload = { type, data }
  [types.SET_AUTH] (state, payload) {
    Logger.log(payload);
    let data = payload.data;

    state.name = data.name;
    // state.id = data.id;
    state.email = data.email;
    state.website = data.website;
    state.insta_handle = data.insta_handle;
    state.bio = data.bio;
    state.imageUrl = data.imageUrl;
    state.fbAuthToken = data.fbAuthToken;
    state.sketches = data.sketches;
    state.followerCount = data.followerCount;
    state.followingCount = data.followingCount;
  },

  // payload = { type, data }
  [types.DELETE_AUTH] (state, payload) {
    Logger.log(payload);
    state.isLoggedIn = false;
    state.name = '';
    state.id = '';
    state.email = '';
    state.website = '';
    state.insta_handle = '';
    state.bio = '';
    state.imageUrl = '';
    state.fbAuthToken = '';
    state.sketches = [];
  },
}
