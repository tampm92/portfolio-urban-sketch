export default {
  isLoggedIn: false,
  id: '',
  email: '',
  name: '',
  imageUrl: '',
  fbAuthToken: '',
  sketches: [],
  followerCount: 0,
  followingCount: 0
};
