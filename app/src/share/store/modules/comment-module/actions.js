import * as types from '../../mutation-types'
import {CommentService} from '../../../services'

export default {
  // actionData = {sketchId, userId, value}
  add(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      CommentService.add(actionData)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },

  // actionData = {commentId}
  delete(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      CommentService.delete(actionData.commentId)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  }
}
