import * as types from '../../mutation-types'
import {FavouriteService} from '../../../services'

// const favouriteService = new FavouriteService();

export default {
  // actionData = { sketchId, userId, value }
  addFavourite(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      FavouriteService.add(actionData)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  }
}
