import authModule from "./auth-module"
import userModule from "./user-module"
import pinModule from "./pin-module"
import sketchModule from "./sketch-module"
import searchModule from "./search-module"
import tagModule from "./tag-module"
import favouriteModule from "./favourite-module"
import likeModule from "./like-module"
import notificationModule from "./notification-module"
import feedModule from "./feed-module"
import followModule from "./follow-module"
import commentModule from "./comment-module"

export default {
  authModule,
  userModule,
  pinModule,
  sketchModule,
  searchModule,
  tagModule,
  favouriteModule,
  likeModule,
  notificationModule,
  feedModule,
  followModule,
  commentModule
};
