import * as types from '../../mutation-types'
import {NotificationService} from '../../../services'

export default {
  // actionData = { userId }
  loadAll(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      NotificationService.getByUserId(actionData.userId)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },
}
