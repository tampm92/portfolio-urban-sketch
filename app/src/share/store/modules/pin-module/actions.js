import * as types from '../../mutation-types'
import {PinService} from '../../../services'

export default {
  // actionData = { query }
  loadAll(context, actionData = {}) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      PinService.getAll(actionData.query)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },
  // actionData = { pins }
  setPins(context, actionData = {}) {
    let payload = {
      type: types.SET_PINS,
      data: actionData.pins
    };

    context.commit(payload);
  },
  // actionData = { pinId, el }
  setPinSelected(context, actionData) {
    return new Promise((resolve, reject) => {
      var pin = {};
      if (actionData.pinId) {
        context.state.pins.forEach(item => {
          if (item.id === actionData.pinId) {
            pin = item;
            return;
          }
        })
      }

      context.commit({
        type: types.SET_PIN_SELECTED,
        data: {
          pin: pin,
          pinId: actionData.pinId,
          el: actionData.el
        }
      });

      return resolve(pin);
    })
  },
  // actionData = { pinId, userId }
  getPinById(context, actionData = {}) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      PinService.getPinById(actionData.pinId, actionData.userId)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },
}
