import * as types from '../../mutation-types'

import Logger from '../../../../utils/logger'

export default {
  // payload = { type, data }
  [types.SET_PINS] (state, payload) {
    Logger.log(payload);
    state.pins = payload.data;
  },
  // payload = { type, data: {pin,pinId,el} }
  [types.SET_PIN_SELECTED] (state, payload) {
    Logger.log(payload);
    state.pinSelected = payload.data.pin;
    state.pinIdSelected = payload.data.pinId;
    state.elPinSelected = payload.data.el;
  }
}
