import * as types from '../../mutation-types'
import {SearchService} from '../../../services'

// const searchService = new SearchService();

export default {
  // data = {query}
  search(context, data) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      SearchService.search(data.query)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })

  }
}
