import * as types from '../../mutation-types'
import {SketchService} from '../../../services'

// const sketchService = new SketchService();

export default {
  // actionData = { sketchId }
  getById(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      SketchService.getById(actionData.sketchId)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },

  // actionData = { query }
  loadAll(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      SketchService.getAll(actionData.query)
        .then(result => {
          let payload = {
            type: types.SET_SKETCHES,
            data: result
          };

          context.commit(payload);
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },

  // actionData = { sketch: Sketch }
  addSketch(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    let sketch = actionData.sketch;
    return new Promise((resolve, reject) => {
      SketchService.add(sketch)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },

  // actionData = { sketch: Sketch }
  updateSketch(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    let sketch = actionData.sketch;
    return new Promise((resolve, reject) => {
      SketchService.update(sketch)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  },

  // actionData = { sketchId: sketchId }
  deleteSketch(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      SketchService.delete(actionData.sketchId)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })
  }
}
