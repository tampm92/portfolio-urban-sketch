import * as types from '../../mutation-types'

import Logger from '../../../../utils/logger'

export default {
  // payload = { type, data }
  [types.SET_SKETCHES] (state, payload) {
    Logger.log(payload);
    state.sketches = payload.data;
  }
}
