import * as types from '../../mutation-types'
import {UserService} from '../../../services'

// const userService = new UserService();

export default {
  // actionData = { userId }
  getUserInfo(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    return new Promise((resolve, reject) => {
      UserService.get(actionData.userId)
        .then(result => {
          let payload = {
            type: types.SET_USER,
            data: result
          };
          context.commit(payload);

          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })

  },
  // actionData = { currentUser }
  addUser(context, actionData) {
    context.commit({
      type: types.BEGIN_LOADING
    }, {
      root: true
    });

    let currentUser = actionData.currentUser;
    return new Promise((resolve, reject) => {
      UserService.add(currentUser)
        .then(result => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });
          return resolve(result);
        })
        .catch(error => {
          context.commit({
            type: types.STOP_LOADING
          }, {
            root: true
          });

          return reject(error);
        })
    })

  }
}
