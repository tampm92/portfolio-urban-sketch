import * as types from '../../mutation-types'

import Logger from '../../../../utils/logger'

export default {
  // payload = { type, data }
  [types.SET_USER] (state, payload) {
    Logger.log(payload);
    let data = payload.data;

    state.name = data.name;
    state.id = data.id;
    state.email = data.email;
    state.website = data.website;
    state.insta_handle = data.insta_handle;
    state.bio = data.bio;
    state.imageUrl = data.imageUrl;
    state.fbAuthToken = data.fbAuthToken;
    state.sketches = data.sketches;
  },
}
