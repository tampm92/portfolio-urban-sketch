export const BEGIN_LOADING = 'BEGIN_LOADING';
export const STOP_LOADING = 'STOP_LOADING';

// global
export const SET_IS_MOBILE_SIZE = 'SET_IS_MOBILE_SIZE';
export const SET_LAYOUT_NAME = 'SET_LAYOUT_NAME';
export const SET_LEFT_SLIDER = 'SET_LEFT_SLIDER';
export const BACK_LEFT_SLIDER = 'BACK_LEFT_SLIDER';
export const SET_CURRENT_LOCATION = 'SET_CURRENT_LOCATION';

// website
// - home page
export const SET_HOME_MAP = 'SET_HOME_MAP';
export const SET_HOME_CURRENT_MARKER = 'SET_HOME_CURRENT_MARKER';
export const PUSH_HOME_MARKER = 'PUSH_HOME_MARKER';
// - search page
export const SET_SEARCH_PAGE_TAGS = 'SET_SEARCH_PAGE_TAGS';

// modules
// - auth
export const STARTUP = 'STARTUP';
export const SET_AUTH = 'SET_AUTH';
export const DELETE_AUTH = 'DELETE_AUTH';
// - user
export const SET_USER = 'SET_USER';
// - pin
export const SET_PINS = 'SET_PINS';
export const SET_PIN_SELECTED = 'SET_PIN_SELECTED';
// - sketch
export const SET_SKETCHES = 'SET_SKETCHES';
// - search
