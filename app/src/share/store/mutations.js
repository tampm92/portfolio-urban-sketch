import * as types from './mutation-types'
import Logger from '../../utils/logger'

export default {
  // payload = { type, data }
  [types.SET_IS_MOBILE_SIZE](state, payload) {
    Logger.log(payload);
    state.isMobileSize = payload.data;
  },
  [types.BEGIN_LOADING](state, payload) {
    Logger.log(payload);
    state.isLoading = true;
  },
  [types.STOP_LOADING](state, payload) {
    Logger.log(payload);
    state.isLoading = false;
  },
  // payload = { type, data }
  [types.SET_LAYOUT_NAME](state, payload) {
    Logger.log(payload);
    state.layoutName = payload.data;
  },
  // payload = { type, data }
  [types.SET_LEFT_SLIDER](state, payload) {
    Logger.log(payload);
    state.nameBackLeftSlider = state.nameLeftSlider;
    state.nameLeftSlider = payload.data;
  },
  // payload = { type, data }
  [types.BACK_LEFT_SLIDER](state, payload) {
    Logger.log(payload);
    state.nameLeftSlider = state.nameBackLeftSlider;
  },
  // payload = { type, data }
  [types.SET_CURRENT_LOCATION](state, payload) {
    Logger.log(payload);
    state.currentLocation = payload.data;
  },

  /**
   * Search page
   */
  // payload = { type, data }
  [types.SET_SEARCH_PAGE_TAGS](state, payload) {
    Logger.log(payload);
    state.searchPageTags = payload.data;
  },

  /**
   * Home page
   */
  // payload = { type, data }
  [types.SET_HOME_MAP](state, payload) {
    Logger.log(payload);
    state.homeMap = payload.data;
  },
  // payload = { type, data }
  [types.SET_HOME_CURRENT_MARKER](state, payload) {
    Logger.log(payload);
    state.homeCurrentMarker = payload.data;
  },
  // payload = { type, data }
  [types.PUSH_HOME_MARKER](state, payload) {
    // Logger.log(payload);
    state.homeMarkers.push(payload.data);
  }
};
