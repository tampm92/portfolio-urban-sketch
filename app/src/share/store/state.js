export default {
  // configs env
  nodeEnv: process.env.NODE_ENV,
  platformEnv: process.env.PLATFORM_ENV,
  applicationTitle: process.env.APP_NAME,
  mailSupport: process.env.MAIL_SUPPORT,
  mapboxAccessToken: process.env.MAPBOX.accessToken,
  mapboxDefaultStyle: process.env.MAPBOX.defaultStyle,
  defaultLocation: process.env.DEFAULT.location,
  // awsAccessKeyId: process.env.AWS.accessKeyId,
  // awsSecretAccessKey: process.env.AWS.secretAccessKey,
  // awsRegion: process.env.AWS.region,

  // website
  maxHeightMobile: 576,
  isMobileSize: false,
  isLoading: false,
  layoutName: 'div',
  nameLeftSlider: '', // currentProfile/currentUpdateProfile/profile/imageCollection/feed/editCurrentFeed/notifications/followers/following
  nameBackLeftSlider: '',
  classMarkerDefault: 'marker',
  classMarkerNormal: 'marker-red',
  classMarkerSelected: 'marker-white',
  classMarkerCurrent: 'marker-you-are-here',
  currentLocation: {longitude: 0, latitude: 0},

  // home page
  homeMap: null,
  homeCurrentMarker: null,
  homeMarkers: [],

  // search page
  searchPageTags: []
}
