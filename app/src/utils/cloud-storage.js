const URL_PRE_SIGNED = process.env.AWS.urlPreSigned;
const URL_S3 = process.env.AWS.urlS3;

const CloudStorage = {
  /**
   *
   * @param {file} file
   * return url
   */
  upload(file) {
    return new Promise((resolve, reject) => {
      fetch(`${URL_PRE_SIGNED}/${file.name}`, {
        method: "GET"
      })
      .then(function(response){
        return response.json();
      })
      .then(function(json){
        return fetch(json.url, {
          method: "PUT",
          headers: {
            'Content-Type': 'application/octet-stream'
          },
          body: file
        }).then(data => {
          if (data.status == 200) {
            return resolve(`${URL_S3}/${file.name}`);
          } else {
            return reject(data.status)
          }
        }).catch(error => {
          return reject(err);
        })
      })
    });
  }
}

export default CloudStorage;
