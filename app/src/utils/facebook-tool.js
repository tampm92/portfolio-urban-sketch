var OpenFB = (function () {
  var loginCallback;
  var loginProcessed;
  var fbAppId = process.env.FACEBOOK.appId;
  var loginURL = 'https://www.facebook.com/dialog/oauth';

  function login(callback, options) {
    var loginWindow, startTime;
    var scope = '';
    var redirectURL = 'https://www.facebook.com/connect/login_success.html';

    function loginWindow_loadStartHandler(event) {
      var url = event.url;
      if (url.indexOf("access_token=") > 0 || url.indexOf("error=") > 0) {
        var timeout = 600 - (new Date().getTime() - startTime);
        setTimeout(function () {
          loginWindow.close();
        }, timeout > 0 ? timeout : 0);
        oauthCallback(url);
      }
    }

    function loginWindow_exitHandler() {
      if (loginCallback && !loginProcessed) loginCallback({
        status: 'user_cancelled'
      });
      loginWindow.removeEventListener('loadstart', loginWindow_loadStartHandler);
      loginWindow.removeEventListener('exit', loginWindow_exitHandler);
      loginWindow = null;
    }

    if (options && options.scope) {
      scope = options.scope;
    }

    loginCallback = callback;
    loginProcessed = false;

    startTime = new Date().getTime();
    loginWindow = window.open(loginURL + '?client_id=' + fbAppId + '&redirect_uri=' + redirectURL +
      '&response_type=token&scope=' + scope, '_top', 'location=no,clearcache=yes');

    loginWindow.addEventListener('loadstart', loginWindow_loadStartHandler);
    loginWindow.addEventListener('exit', loginWindow_exitHandler);
  };

  function oauthCallback(url) {
    var queryString, obj;

    loginProcessed = true;
    if (url.indexOf("access_token=") > 0) {
      queryString = url.substr(url.indexOf('#') + 1);
      obj = parseQueryString(queryString);
      if (loginCallback) loginCallback({
        status: 'connected',
        authResponse: {
          accessToken: obj['access_token']
        }
      });
    } else if (url.indexOf("error=") > 0) {
      queryString = url.substring(url.indexOf('?') + 1, url.indexOf('#'));
      obj = parseQueryString(queryString);
      if (loginCallback) loginCallback({
        status: 'not_authorized',
        error: obj.error
      });
    } else {
      if (loginCallback) loginCallback({
        status: 'not_authorized'
      });
    }
  };

  function logout(callback) {
    if (callback) {
      callback();
    }
  }

  function parseQueryString(queryString) {
    var qs = decodeURIComponent(queryString),
      obj = {},
      params = qs.split('&');
    params.forEach(function (param) {
      var splitter = param.split('=');
      obj[splitter[0]] = splitter[1];
    });
    return obj;
  };

  return {
    login: login,
    logout: logout
  }
}());

const FacebookTool = {
  login() {
    return new Promise((resolve, reject) => {
      if (process.env.PLATFORM_ENV === 'cordova') {
        OpenFB.login(
          (response) => {
            if (response.status === 'connected') {
              return resolve(response)
            } else {
              return reject(response);
            }
          }, {
            scope: 'email'
          });
      } else {
        FB.login(
          (response) => {
            return resolve(response)
          }, {
            scope: "email"
          }
        );
      }
    })
  },

  logout() {
    return new Promise((resolve, reject) => {
      if (process.env.PLATFORM_ENV === 'cordova') {
        OpenFB.logout(
          () => {
            return resolve(response)
          });
      } else {
        FB.getLoginStatus(response => {
          if (response && response.status === "connected") {
            FB.logout(function (response) {
              return resolve('Logout success!')
            });
          } else {
            return reject('User not yet login')
          }
        });
      }
    })
  },
}

export default FacebookTool;
