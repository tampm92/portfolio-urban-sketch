var mFeedByUserId = {
  "favourites": {
    "00864172-2daf-45b1-89d1-f3029179159a": {
      "pin_info": {
        "id": "00864172-2daf-45b1-89d1-f3029179159a",
        "name": "4 District",
        "longitude": 106.698,
        "latitude": 10.7476,
        "created_at": "2018-12-10T16:52:44.000Z",
        "updated_at": null
      },
      "sketches": [{
        "id": "01ffe4e3-78f6-47d1-9e1b-236fd97d0a35",
        "name": "Saigon Royal Residence",
        "url": "/static/images/fake/saigonroyal.jpg",
        "user_id": "1905104926222686",
        "pin_id": "00864172-2daf-45b1-89d1-f3029179159a",
        "created_at": "2018-11-27T20:42:45.000Z",
        "updated_at": "2018-12-10T16:52:44.000Z",
        "description": null,
        "likes_count": 0,
        "comments_count": 0
      }]
    },
  },
  "followedUsers": {
    "10211688549781585": {
      "userInfo": {
        "user_id": "10211688549781585",
        "name": "Kim Han",
        "email": "kimhan@gmail.com",
        "image_url": "/static/images/icon-profile.png"
      },
      "sketches": [{
        "id": "a9e715a1-385f-45ed-ae07-873d414fcab5",
        "name": "Phu Tho",
        "url": "/static/images/fake/nha-thi-dau-phu-tho.jpg",
        "pin_id": "02ff4363-4c10-4f17-b61d-c9ad582566b8",
        "likes_count": 1,
        "comments_count": 0
      }]
    }
  }
}

var mFeedSetting = {
  "followed_users": [{
    "following_user_id": "10211688549781585",
    "follower_user_id": "1905104926222686",
    "value": 1,
    "created_at": "2018-12-06T21:35:11.000Z",
    "updated_at": "2018-12-25T07:16:13.000Z",
    "block_follower": 0,
    "follower_user_name": "Kim Han",
    "image_url": "/static/images/icon-profile.png",
    "type": "user"
  }],
  "followed_locations": [{
      "id": "c3fed1f0-6866-42a2-b8b4-2121fa3c88b2",
      "user_id": "1905104926222686",
      "sketch_id": "",
      "pin_id": "00864172-2daf-45b1-89d1-f3029179159a",
      "value": "1",
      "created_at": "2019-01-31T22:46:24.000Z",
      "updated_at": null,
      "type": "location",
      "name": "pin test 116"
    },
    {
      "id": "054d23c9-b1fb-4fd7-84e4-9e2d3351510b",
      "user_id": "1905104926222686",
      "sketch_id": "",
      "pin_id": "85d5506a-6b14-4e37-aba1-3e365c6adc08",
      "value": "1",
      "created_at": "2018-12-28T17:52:44.000Z",
      "updated_at": null,
      "type": "location",
      "name": null
    },
    {
      "id": "d93b34cc-5830-48ba-b4ed-43d1d298324c",
      "user_id": "1905104926222686",
      "sketch_id": "",
      "pin_id": "8f8d732a-b4bd-4302-81ac-04f6a5995905",
      "value": "1",
      "created_at": "2018-12-28T17:55:59.000Z",
      "updated_at": null,
      "type": "location",
      "name": null
    },
    {
      "id": "b50470f7-b9e1-4865-a5a4-f13ae3c975a4",
      "user_id": "1905104926222686",
      "sketch_id": "",
      "pin_id": "99c7d747-5f1c-4c46-95bd-948cee534fed",
      "value": "1",
      "created_at": "2018-12-21T08:52:02.000Z",
      "updated_at": null,
      "type": "location",
      "name": null
    },
    {
      "id": "682d035d-1444-4dad-ab19-e9e78d812118",
      "user_id": "1905104926222686",
      "sketch_id": "",
      "pin_id": "e3aee693-820d-4fca-9f4c-bece325e6735",
      "value": "1",
      "created_at": "2018-12-28T10:03:49.000Z",
      "updated_at": null,
      "type": "location",
      "name": "tes 602 name"
    },
    {
      "id": "492c099f-e65a-408e-869f-6365213bea56",
      "user_id": "1905104926222686",
      "sketch_id": "",
      "pin_id": "ee6b1f33-1b58-48e9-bfd9-46eca00a24b0",
      "value": "1",
      "created_at": "2018-12-10T19:03:18.000Z",
      "updated_at": "2018-12-22T19:03:15.000Z",
      "type": "location",
      "name": "test 408 name"
    }
  ]
}

export default {
  findByUserId(userId) {
    return mFeedByUserId;
  },

  findFeedSetting(userId) {
    return mFeedSetting;
  }
};
