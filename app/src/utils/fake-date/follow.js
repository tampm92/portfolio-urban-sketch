var mFollowers = [
  {
      "following_user_id": "1905104926222686",
      "follower_user_id": "10211688549781585",
      "value": 0,
      "created_at": "2018-12-07T17:05:21.000Z",
      "updated_at": "2018-12-28T10:51:32.000Z",
      "block_follower": 1,
      "following_user_name": "Kim Han",
      "image_url": "/static/images/icon-profile.png"
  }
]

var mFollowing = [
  {
      "following_user_id": "10211688549781585",
      "follower_user_id": "1905104926222686",
      "value": 1,
      "created_at": "2018-12-06T21:35:11.000Z",
      "updated_at": "2018-12-25T07:16:13.000Z",
      "block_follower": 0,
      "follower_user_name": "Kim Han",
      "image_url": "/static/images/icon-profile.png",
      "type": "user"
  }
]

export default {
  getFollowersByUserId(userId) {
    return mFollowers;
  },

  getFollowingByUserId(userId){
    return mFollowing;
  }
};
