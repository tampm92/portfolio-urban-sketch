var mNotificationFeed = {
  "feed": [
      {
          "created_at": "2018-12-28T11:00:54.000Z",
          "type": "like",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-28T10:51:32.000Z",
          "type": "follow",
          "id": "10211688549781585",
          "name": "Kim Han",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-28T10:03:49.000Z",
          "type": "favourite",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-22T19:03:15.000Z",
          "type": "favourite",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-21T09:15:02.000Z",
          "type": "favourite",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-21T08:59:25.000Z",
          "type": "favourite",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-21T08:52:02.000Z",
          "type": "favourite",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-21T08:21:00.000Z",
          "type": "like",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-12T23:15:15.000Z",
          "type": "comment",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-12T22:31:12.000Z",
          "type": "comment",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-12T22:31:08.000Z",
          "type": "comment",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-12T06:43:23.000Z",
          "type": "comment",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-12T06:43:16.000Z",
          "type": "comment",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-08T22:19:14.000Z",
          "type": "like",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-12-05T17:24:06.000Z",
          "type": "like",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      },
      {
          "created_at": "2018-11-24T20:54:58.000Z",
          "type": "like",
          "id": "1905104926222686",
          "name": "Tam Phanminh",
          "image_url": "/static/images/icon-profile.png"
      }
  ],
  "summary": {
      "followers": 2,
      "following": 2
  }
}

export default {
  getNotificationFeedByUserId(userId) {
    return mNotificationFeed;
  }
};
