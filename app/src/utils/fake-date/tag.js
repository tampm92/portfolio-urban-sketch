var mTags = [
  {
      "tag": "another tag",
      "created_at": "2018-12-26T22:13:22.000Z"
  },
  {
      "tag": "USA",
      "created_at": "2018-11-21T04:19:52.000Z"
  },
  {
      "tag": "Viet Nam",
      "created_at": "2018-11-02T02:50:47.000Z"
  }
]

export default {
  getAll() {
    return mTags;
  }
};
