var mUserById = [{
  "user_info": [{
    "id": "10211688549781585",
    "fb_auth_token": "",
    "name": "Kim Han",
    "email": "kimhan@gmail.com",
    "instagram_token": null,
    "image_url": "/static/images/icon-profile.png",
    "created_at": "2018-10-31T16:45:34.000Z",
    "updated_at": "2019-02-10T05:37:52.000Z",
    "tutorial_seen": 1,
    "bio": "undefined",
    "insta_handle": "undefined",
    "website": "undefined"
  }],
  "followers": [
    {
      "id": "10211688549781585",
      "name": "Kim Han",
      "image_url": "/static/images/icon-profile.png"
    },
    {
      "id": "1905104926222686",
      "name": "Tam Phanminh",
      "image_url": "/static/images/icon-profile.png"
    }
  ],
  "sketches": [
  ],
  "follower_count": 3,
  "following_count": 4
}, {
  "user_info": [{
    "id": "1905104926222686",
    "fb_auth_token": "",
    "name": "Tam Phanminh",
    "email": "phanminhtam1992@gmail.com",
    "instagram_token": null,
    "image_url": "/static/images/icon-profile.png",
    "created_at": "2018-10-30T11:15:37.000Z",
    "updated_at": "2019-02-11T18:35:58.000Z",
    "tutorial_seen": 1,
    "bio": "undefined",
    "insta_handle": "undefined",
    "website": "undefined"
  }],
  "followers": [{
    "id": "10211688549781585",
    "name": "Kim Han",
    "image_url": "/static/images/icon-profile.png"
  }],
  "sketches": [
    {
      "id": "01ffe4e3-78f6-47d1-9e1b-236fd97d0a35",
      "name": "Saigon Royal Residence",
      "url": "/static/images/fake/saigonroyal.jpg",
      "user_id": "1905104926222686",
      "pin_id": "00864172-2daf-45b1-89d1-f3029179159a",
      "created_at": "2018-11-27T20:42:45.000Z",
      "updated_at": "2018-12-10T16:52:44.000Z",
      "description": null
    },
    {
      "id": "0ffe675c-7c77-4e19-924b-b201d6d897ce",
      "name": "Celadon City",
      "url": "/static/images/fake/celadon-city.jpg",
      "user_id": "1905104926222686",
      "pin_id": "0f572d15-9d1a-4b98-baef-82ea3f214364",
      "created_at": "2018-12-09T01:39:36.000Z",
      "updated_at": null,
      "description": null
    },
    {
      "id": "9f26d69a-8719-47fb-b710-7eaf2c06748a",
      "name": "Lu Gia Plaza",
      "url": "/static/images/fake/lu-gia-plaza.jpg",
      "user_id": "1905104926222686",
      "pin_id": "0a48cf04-a6ea-4798-b94f-c9e6330189b9",
      "created_at": "2018-11-21T13:30:32.000Z",
      "updated_at": null,
      "description": null
    },
    {
      "id": "a9e715a1-385f-45ed-ae07-873d414fcab5",
      "name": "Phu Tho",
      "url": "/static/images/fake/nha-thi-dau-phu-tho.jpg",
      "user_id": "1905104926222686",
      "pin_id": "02ff4363-4c10-4f17-b61d-c9ad582566b8",
      "created_at": "2018-12-28T11:15:30.000Z",
      "updated_at": null,
      "description": null
    },
  ],
  "follower_count": 2,
  "following_count": 2
}]

export default {
  login() {
    return {id: '1905104926222686'}
  },

  getRandomUser() {
    return mUserById[1];
  },

  findById(id) {
    let user = mUserById.find(u => u.user_info[0].id === id);
    return user
  }
};
