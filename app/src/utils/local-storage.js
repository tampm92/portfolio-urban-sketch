import store from 'store';

const LocalStorage = {
  set(key, value) {
    store.set(key, value);
  },

  get(key) {
    return store.get(key);
  },

  remove(key) {
    store.remove(key);
  },

  isKey(key) {
    var value = store.get(key);
    return !!value;
  }
}

export default LocalStorage;
