import mapboxgl from 'mapbox-gl'
import MapboxGeocoder from 'mapbox-gl-geocoder';
import 'mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

const urlSearch = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';

const MapTool = {
  /**
   *
   * @param {string} accessToken
   * @param {json map option} config
   * return element map
   */
  init(accessToken, config, isAddSearch = false) {
    mapboxgl.accessToken = accessToken;
    var map = new mapboxgl.Map(config);

    if (isAddSearch) {
      map.addControl(new MapboxGeocoder({
        accessToken: mapboxgl.accessToken
      }));
    }

    return map;
  },

  /**
   *
   * @param {*} accessToken
   * @param {*} searchString
   * return list object: {
            "id": "locality.8590904765747387",
            "type": "Feature",
            "place_type": [
                "locality"
            ],
            "relevance": 1,
            "properties": {
                "wikidata": "Q1022913"
            },
            "text": "Haizhu Qu",
            "place_name": "Haizhu Qu, Guangzhou Shi, Guangdong, China",
            "matching_text": "Haizhu",
            "matching_place_name": "Haizhu, Guangzhou Shi, Guangdong, China",
            "bbox": [
                113.236784,
                23.045154,
                113.414922,
                23.1167
            ],
            "center": [
                113.26606,
                23.09876
            ],
            "geometry": {
                "type": "Point",
                "coordinates": [
                    113.26606,
                    23.09876
                ]
            },
            "context": [
                {
                    "id": "place.10664098433323280",
                    "wikidata": "Q16572",
                    "text": "Guangzhou Shi"
                },
                {
                    "id": "region.13986790090334100",
                    "short_code": "CN-44",
                    "wikidata": "Q15175",
                    "text": "Guangdong"
                },
                {
                    "id": "country.17713928879157440",
                    "short_code": "cn",
                    "wikidata": "Q148",
                    "text": "China"
                }
            ]
        }
   */
  search(accessToken, searchString) {
    return new Promise((resolve, reject) => {
      var url = `${urlSearch}${searchString}.json?access_token=${accessToken}`
      fetch(url)
        .then(function(response) {
          if (!response.ok) {
            throw Error(response.statusText);
          }
          // Read the response as json.
          return response.json();
        })
        .then(function(responseAsJson) {
          return resolve(responseAsJson.features);
        })
        .catch(function(error) {
          return reject(error);
        });
    })
  },

  /**
   *
   * @param {map object} map
   * @param {[lng, lat]} location
   */
  flyTo(map, location) {
    map.flyTo({
      center: location
    });
  },

  /**
   *
   * @param {map} map
   * @param {name, path, location, event} marker
   * return marker
   */
  addMarker(map, marker) {
    var el = document.createElement('div');
    el.id = `pin-${marker.name}`
    el.className = marker.class;

    el.addEventListener('click', () => {
      marker.event(el, marker.name);
    });

    // add marker to map
    return new mapboxgl.Marker(el)
      .setLngLat(marker.location)
      .addTo(map);
  },

  addMarkerOnMove(map, marker, callback) {
    map.on('move', function () {
      var center = map.getCenter();
      marker.setLngLat([center.lng, center.lat]);

      return callback([center.lng, center.lat]);
    });
  },

  clearMarkers(makers) {
    // var makers = document.querySelectorAll('id^="pin-"');

    for (let index = 0; index < makers.length; index++) {
      const element = makers[index];

      element.remove();
    }
  },

  triggerClickMarkerByName(name) {
    var elMarker = document.getElementById(`pin-${name}`);
    if (elMarker) elMarker.click();
  },

  /**
   * this.getCurrentLocation()
      .then((location) => {
        console.log(location)
      })
      .catch(error => {
        Notification.error('Location', error.message || error);
      });
   */
  getCurrentLocation() {
    var options = {
      enableHighAccuracy: true,
      timeout: 60000,
      maximumAge: 0
    };

    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            console.log(position)
            return resolve(position.coords);
          },
          (error) => {
            return reject(error);
          },
          options
        );
      } else {
        return reject('Geolocation is not supported by this browser.');
      }
    });
  }
}

export default MapTool;
