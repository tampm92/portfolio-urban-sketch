import Vue from 'vue'

const Notification = {
  notify(type, title, text) {
    Vue.notify({
      group: 'notification-template-normal',
      type: type ? type : 'default',
      title: title ? title : 'Urban Sketch',
      text: text ? text : 'Hello',
    })
  },

  /**
   *
   * @param {type, click, clickName} data // All is required
   */
  notifyTemplateV1({
    type = 'default',
    title = '',
    text = '',
    data = {}
  }) {
    Vue.notify({
      group: 'notification-template-v1',
      type: type,
      title: title,
      text: text,
      data: data
    })
  },

  notifyTemplateV2({
    type = 'default',
    title = '',
    text = '',
  }) {
    Vue.notify({
      group: 'notification-template-v2',
      type: type,
      title: title,
      text: text
    })
  },

  success(title, text) {
    this.notifyTemplateV2({
      type: 'success',
      title: title,
      text: text
    });
  },
  warn(title, text) {
    this.notifyTemplateV2({
      type: 'warn',
      title: title,
      text: text
    });
  },
  error(title, text) {
    this.notifyTemplateV2({
      type: 'error',
      title: title,
      text: text
    });
  }
}

export default Notification;
